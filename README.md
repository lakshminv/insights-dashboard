
# Hitoe Insights Dashboard

### What's included:

- Base UI framework is reused from ngx-admin
- Uses Angular 2
- Bootstrap 4+ & SCSS
- Responsive layout
- High resolution


### Demo

<a target="_blank" href="http://34.203.80.130/hitoe/">Live Demo</a>

 

