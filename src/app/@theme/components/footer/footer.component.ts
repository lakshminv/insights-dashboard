import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <div> </div>
    <div class="socials">
      <i class="fa fa-copyright" aria-hidden="true"></i> NTT Data Services, 2017
    </div>
  `,
})
export class FooterComponent {
}
